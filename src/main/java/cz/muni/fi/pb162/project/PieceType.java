package cz.muni.fi.pb162.project;

/**
 * Enum representing chess piece type
 * 
 * @author Jakub Mihálik
 */
public enum PieceType {
    PAWN,
    KNIGHT,
    BISHOP,
    ROOK,
    QUEEN,
    KING,
    DRAUGHTS_MAN,
    DRAUGHTS_KING;
}
