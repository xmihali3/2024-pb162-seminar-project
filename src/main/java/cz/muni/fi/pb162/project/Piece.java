package cz.muni.fi.pb162.project;

import cz.muni.fi.pb162.project.moves.Diagonal;
import cz.muni.fi.pb162.project.moves.Jump;
import cz.muni.fi.pb162.project.moves.Knight;
import cz.muni.fi.pb162.project.moves.Move;
import cz.muni.fi.pb162.project.moves.Pawn;
import cz.muni.fi.pb162.project.moves.Straight;
import org.apache.commons.lang3.exception.UncheckedException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.Objects;

import static java.util.Objects.hash;

/**
 * Class representing chess piece.
 *
 * @author Jakub Mihálik
 */
public class Piece implements Prototype<Piece>{
    private final Color color;
    private final PieceType pieceType;
    private static final Map<Piece, String> ICON_CACHE = new HashMap<>();
    private static final String FANCY_ICONS_FILE = "pieces.txt";
    private static final AtomicLong IDGENERATOR = new AtomicLong();
    private final long id;
    private final List<Move> movementStrategies;

    /**
     * Calculates all movement strategies of the piece
     *
     * @param pieceType type of the piece
     * @return all possible movement strategies of the piece
     */
    private List<Move> getAllMovementStrategies(PieceType pieceType){
        return switch (pieceType) {
            case KING ->List.of(new Straight(1), new Diagonal(1));
            case QUEEN ->List.of(new Straight(), new Diagonal());
            case BISHOP ->List.of(new Diagonal());
            case ROOK ->List.of(new Straight());
            case KNIGHT ->List.of(new Knight());
            case PAWN ->List.of(new Pawn());
            case DRAUGHTS_KING ->List.of(new Diagonal(1), new Jump());
            case DRAUGHTS_MAN ->List.of(new Diagonal(1, true), new Jump(true));
        };
    }
    /**
     * creates a piece
     *
     * @param color color of the piece
     * @param pieceType type of the piece
     */
    public Piece(Color color, PieceType pieceType) {
        this.color = Objects.requireNonNull(color, "Color must not be null");
        this.pieceType = Objects.requireNonNull(pieceType, "Piece type must not be null");
        id = IDGENERATOR.incrementAndGet();
        movementStrategies = getAllMovementStrategies(pieceType);
    }

    /**
     * creates a copy of a piece
     *
     * @param piece copied piece
     */
    public Piece(Piece piece) {
        this(Objects.requireNonNull(piece, "Piece must not be null").getColor(), piece.getPieceType());
    }

    public Color getColor() {
        return color;
    }

    public PieceType getPieceType() {
        return pieceType;
    }

    public long getId() {
        return id;
    }

    public List<Move> getMovementStrategies() {
        return movementStrategies;
    }

    private void makeFancyIcon() {
        try(BufferedReader br = new BufferedReader(new FileReader(FANCY_ICONS_FILE))) {
            String line = br.readLine();
            while (line != null) {
                String[] info = line.split(":");
                ICON_CACHE.put(new Piece(Color.valueOf(info[0]), PieceType.valueOf(info[1])), info[2]);
                line = br.readLine();
            }
        } catch (Exception e) {
            throw new UncheckedException(e);
        }
    }
    @Override
    public String toString() {
        if (!ICON_CACHE.containsKey(this)) {
            makeFancyIcon();
        }
        return ICON_CACHE.getOrDefault(this, this.pieceType.toString().substring(0, 1));
    }

    @Override
    public Piece makeClone() {
        return new Piece(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (this.getClass() == obj.getClass()) {
            Piece piece = (Piece) obj;
            return pieceType.equals(piece.getPieceType()) && color.equals(piece.getColor());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return hash(getColor(), getPieceType());
    }

    /**
     * Calculates all currently possible moves of the piece
     *
     * @param game game where the piece is used
     * @return all possible moves of the piece
     */
//    public Set<Position> getAllPossibleMoves(Game game) {
//        Set<Position> possibleMoves = new HashSet<>();
//        Position position = game.getBoard().findCoordinatesOfPieceById(id);
//        for (Move movementStrategy : movementStrategies) {
//            possibleMoves.addAll(movementStrategy.getAllowedMoves(game, position));
//        }
//        return possibleMoves;
//    }
    public Set<Position> getAllPossibleMoves(Game game) {
        Set<Position> possibleMoves = new HashSet<>();
        Position position = game.getBoard().findCoordinatesOfPieceById(id);
        movementStrategies.stream()
                .map(strategy -> strategy.getAllowedMoves(game, position))
                .flatMap(Set::stream)
                .forEach(possibleMoves::add);
        return possibleMoves;
    }
}
