package cz.muni.fi.pb162.project;

/**
 * abstract class for building games
 *
 * @param <T> what game mode are we building
 * @author Jakub Mihálik
 */
public abstract class GameBuilder<T> {
    private Player player1;
    private Player player2;
    private final Board board = new Board();

    /**
     * adds a player to the game
     *
     * @param player new player
     * @return this object
     */
    public GameBuilder<T> addPlayer(Player player) {
        if (player1 == null) {
            player1 = player;
        } else {
            player2 = player;
        }
        return this;
    }

    /**
     * adds a piece to the board
     *
     * @param pos position of the piece
     * @param piece piece added to the board
     * @return this object
     */
    public GameBuilder<T> addPieceToBoard(Position pos, Piece piece) {
        board.putPieceOnBoard(pos, piece);
        return this;
    }

    /**
     * method for building games
     *
     * @return specific game mode object
     */
    public abstract T build();

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public Board getBoard() {
        return board;
    }
}
