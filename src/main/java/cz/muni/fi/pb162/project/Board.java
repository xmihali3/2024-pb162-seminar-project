package cz.muni.fi.pb162.project;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Comparator;

/**
 * Class representing chessboard.
 *
 * @author Jakub Mihálik
 */
public class Board implements Prototype<Board>{
    public static final int SIZE = 8;
    private final Piece[][] board;

    /**
     * creates a chessboard
     *
     * @param size number of fields in each row/column
     */
    public Board(int size) {
        board = new Piece[size][size];
    }

    /**
     * creates a chessboard with default size
     */
    public Board() {
        this(SIZE);
    }

    public int getSize() {
        return board.length;
    }

    /**
     *
     * @return boolean representing whether the position is valid
     */
    public boolean inRange(Position position) {
        return 0 <= position.column() && position.column() < getSize()
                && 0 <= position.line() && position.line() < getSize();
    }

    /**
     *
     * @return boolean whether the position is empty
     */
    public boolean isEmpty(Position position) {
        if (!inRange(position)) {
            return true;
        }
        return board[position.column()][position.line()] == null;
    }

    /**
     *
     * @return piece in given position
     */
    public Piece getPiece(Position position) {
        if (isEmpty(position)) {
            return null;
        }
        return board[position.column()][position.line()];
    }

    /**
     * puts a piece on the board in given position
     */
    public void putPieceOnBoard(Position position, Piece piece) {
        if (inRange(position)) {
            board[position.column()][position.line()] = piece;
        }
    }

    /**
     *
     * @param id id of a piece
     * @return Position of the piece with the given id / returns null if the piece is not on the board
     */
    public Position findCoordinatesOfPieceById(long id) {
        for (int i = 0; i < getSize(); i++) {
            for (int j = 0; j < getSize(); j++) {
                if (board[i][j] != null && board[i][j].getId() == id) {
                    return new Position(i, j);
                }
            }
        }
        return null;
    }

    /**
     * Returns array of all pieces on the board
     *
     * @return all pieces on the board
     */
//    public Piece[] getAllPiecesFromBoard() {
//        List<Piece> pieces = new ArrayList<>();
//        for (int i = 0; i < board.length; i++) {
//            for (int j = 0; j < board.length; j++) {
//                if (board[i][j] != null) {
//                    pieces.add(getPiece(new Position(i, j)));
//                }
//            }
//        }
//        return pieces.toArray(new Piece[0]);
//    }
    public Piece[] getAllPiecesFromBoard() {
        return Arrays.stream(board)
                .flatMap(Arrays::stream)
                .filter(Objects::nonNull)
                .toArray(Piece[]::new);
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder("  ");
        for (int i = 0; i < board.length; i++) {
            string.append("  ").append((char)('A' + i)).append(" ");
        }
        string.append(System.lineSeparator()).append("  ").append("----".repeat(board.length));
        for (int i = 0; i < board.length; i++) {
            string.append(System.lineSeparator()).append(board.length - i).append(" |");
            for (int j = 0; j < board.length; j++) {
                string.append(" ");
                Piece piece = getPiece(new Position(i, j));
                if (piece != null) {
                    string.append(piece).append(" |");
                } else {
                    string.append("  |");
                }
            }
            string.append(System.lineSeparator()).append("  ").append("----".repeat(board.length));
        }
        return string.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (this.getClass() == obj.getClass()) {
            Board board1 = (Board) obj;
            return Arrays.deepEquals(getAllPiecesFromBoard(), board1.getAllPiecesFromBoard());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Arrays.deepHashCode(board);
    }

    /**
     * Removes all given pieces from the board
     *
     * @param toRemove set of pieces to remove from the board
     * @return set of positions of the removed pieces
     */
    public Set<Position> removePieces(Set<Piece> toRemove) {
        Piece[] allPieces = getAllPiecesFromBoard();
        Set<Position> removedPieces = new HashSet<>();
        for (Piece piece : allPieces) {
            if (toRemove.contains(piece)) {
                Position position = findCoordinatesOfPieceById(piece.getId());
                removedPieces.add(position);
                board[position.column()][position.line()] = null;
            }
        }
        return removedPieces;
    }

    @Override
    public Board makeClone() {
        Board newBoard = new Board(getSize());
        for (int i = 0; i < getSize(); i++) {
            newBoard.board[i] = Arrays.copyOf(board[i], getSize());
        }
        return newBoard;
    }

    /**
     * Adds all positions with a piece on the board sorted inversely
     *
     * @param positions set to which will the positions be added
     */
    protected void addOccupiedPositions(SortedSet<Position> positions) {
        for (int i = 0; i < getSize(); i++) {
            for (int j = 0; j < getSize(); j++) {
                Position pos = new Position(i, j);
                if (getPiece(pos) != null) {
                    positions.add(pos);
                }
            }
        }
    }

    /**
     * Finds all occupied positions on the board using PositionInverseComparator.
     *
     * @return positions with a piece sorted inversely
     */
    public SortedSet<Position> getOccupiedPositionsA() {
        SortedSet<Position> occupiedPositions = new TreeSet<>(new PositionInverseComparator());
        addOccupiedPositions(occupiedPositions);
        return occupiedPositions;
    }

    /**
     * Finds all occupied positions on the board using inverse ordering as anonymous class inside this method.
     *
     * @return positions with a piece sorted inversely
     */
    public SortedSet<Position> getOccupiedPositionsB() {
        SortedSet<Position> occupiedPositions = new TreeSet<>(new Comparator<Position>() {
            @Override
            public int compare(Position position1, Position position2) {
                return position2.compareTo(position1);
            }
        });
        addOccupiedPositions(occupiedPositions);
        return occupiedPositions;
    }

    /**
     * Finds all occupied positions on the board using inverse ordering as lambda expression.
     *
     * @return positions with a piece sorted inversely
     */
    public SortedSet<Position> getOccupiedPositionsC() {
        SortedSet<Position> occupiedPositions = new TreeSet<>((pos1, pos2) -> pos2.compareTo(pos1));
        addOccupiedPositions(occupiedPositions);
        return occupiedPositions;
    }

    /**
     * Finds all occupied positions on the board using inverse ordering obtained from the natural ordering
     * without any additional line of code.
     *
     * @return positions with a piece sorted inversely
     */
    public SortedSet<Position> getOccupiedPositionsD() {
        SortedSet<Position> occupiedPositions = new TreeSet<>(Collections.reverseOrder());
        addOccupiedPositions(occupiedPositions);
        return occupiedPositions;
    }
}
