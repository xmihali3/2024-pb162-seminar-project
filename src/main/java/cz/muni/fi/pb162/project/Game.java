package cz.muni.fi.pb162.project;

import cz.muni.fi.pb162.project.exceptions.EmptySquareException;
import cz.muni.fi.pb162.project.exceptions.InvalidFormatOfInputException;
import cz.muni.fi.pb162.project.exceptions.NotAllowedMoveException;

import java.util.Scanner;

import static java.util.Objects.hash;

/**
 * Class representing game
 *
 * @author Jakub Mihálik
 */
public abstract class Game implements Playable {
    private final Player playerOne;
    private final Player playerTwo;
    private StateOfGame stateOfGame;
    private final Board board;
    private int rounds = 0;

    /**
     * Creates a game
     *
     * @param playerOne Player of the game
     * @param playerTwo Other player of the game
     */
    public Game(Player playerOne, Player playerTwo) {
        this(playerOne, playerTwo, new Board());
    }

    protected Game(Player playerOne, Player playerTwo, Board board) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        stateOfGame = StateOfGame.PLAYING;
        this.board = board;
    }

    /**
     * Tells you which player's turn it is
     *
     * @return Player whose turn it is
     */
    public Player getCurrentPlayer() {
        if (playerOne.color() == Color.WHITE && rounds % 2 == 0
           || playerTwo.color() == Color.WHITE && rounds % 2 != 0) {
            return playerOne;
        }
        return playerTwo;
    }

    public Player getPlayerOne() {
        return playerOne;
    }

    public Player getPlayerTwo() {
        return playerTwo;
    }

    public StateOfGame getStateOfGame() {
        return stateOfGame;
    }

    public void setStateOfGame(StateOfGame stateOfGame) {
        this.stateOfGame = stateOfGame;
    }

    public Board getBoard() {
        return board;
    }

    @Override
    public void move(Position oldPosition, Position newPosition) {
        if (!board.isEmpty(oldPosition) && board.inRange(newPosition)) {
            board.putPieceOnBoard(newPosition, board.getPiece(oldPosition));
            board.putPieceOnBoard(oldPosition, null);
        }
    }

    protected abstract void updateStatus();

    @Override
    public void play()  throws EmptySquareException, NotAllowedMoveException {
        while (stateOfGame == StateOfGame.PLAYING) {
            System.out.println(board.toString());
            Position oldPosition = getInputFromPlayer();
            Position newPosition = getInputFromPlayer();
            if (getBoard().getPiece(oldPosition) == null) {
                throw new EmptySquareException("There is no piece on the first position.");
            }
            if (!getBoard().inRange(newPosition)) {
                throw new EmptySquareException("The second position is not on the board.");
            }
            if (!getBoard().getPiece(oldPosition).getAllPossibleMoves(this).contains(newPosition)) {
                throw new NotAllowedMoveException();
            }
            move(oldPosition, newPosition);
            rounds += 1;
            updateStatus();
        }
    }

    private static final Scanner SCANNER = new Scanner(System.in);

    private Position getInputFromPlayer() {
        try {
            var position = SCANNER.next().trim();
            char column = position.charAt(0);
            int line = Integer.parseInt(String.valueOf(position.charAt(1)));
            return new Position(column, line);
        } catch (NumberFormatException | StringIndexOutOfBoundsException e) {
            throw new InvalidFormatOfInputException("Invalid input format. Use <char><int> <char><int>");
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof Game game) {
            return playerOne == game.getPlayerOne() && playerTwo == game.getPlayerTwo();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return hash(getPlayerOne(), getPlayerTwo());
    }
}
