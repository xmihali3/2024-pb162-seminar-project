package cz.muni.fi.pb162.project;

/**
 * Enum representing piece color
 * 
 * @author Jakub Mihálik
 */
public enum Color {
    BLACK,
    WHITE;

    /**
     *
     * @return Opposite color
     */
    public Color getOppositeColor() {
        if (this == BLACK) {
            return WHITE;
        }
        return BLACK;
    }
}
