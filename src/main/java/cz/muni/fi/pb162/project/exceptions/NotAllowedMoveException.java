package cz.muni.fi.pb162.project.exceptions;

/**
 * Exception representing invalid move
 *
 * @author Jakub Mihálik
 */
public class NotAllowedMoveException extends Exception {

    /**
     * Constructor for new NotAllowedMoveException with predetermined message
     */
    public NotAllowedMoveException() {
        super("The move is not allowed.");
    }

    /**
     * Constructor for new NotAllowedMoveException with desired message
     *
     * @param message desired error message
     */
    public NotAllowedMoveException(String message) {
        super(message);
    }
}