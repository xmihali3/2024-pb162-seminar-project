package cz.muni.fi.pb162.project;

/**
 * Class representing player.
 *
 * @author Jakub Mihálik
 * @param name name of a player
 * @param color color of player's pieces
 */
public record Player(String name, Color color) {

    @Override
    public String toString() {
        return name + "-" + color;
    }
}
