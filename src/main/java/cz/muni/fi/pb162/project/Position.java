package cz.muni.fi.pb162.project;

/**
 * Class representing chessboard position.
 *
 * @author Jakub Mihálik
 * @param column column number on a board
 * @param line row number on a board
 */
public record Position(int column, int line)  implements Comparable<Position> {

    private static final int UNICODE_A = 97;

    /**
     * creates position from char and int
     *
     * @param column column number on a board
     * @param line row number on a board
     */
    public Position(char column, int line) {
        this(column - UNICODE_A, --line);
    }

    /**
     * 
     * @return a string representing a readable position for players
     */
    @Override
    public String toString() {
        return "" + (char) (column + UNICODE_A) + (line + 1);
    }

    /**
     *
     * @param position another position on a board
     * @return summed position of two positions
     */
    public Position add(Position position) {
        return new Position(column + position.column, line + position.line);
    }

    @Override
    public int compareTo(Position position) {
        if (line == position.line()) {
            return Integer.compare(column, position.column());
        }
        return Integer.compare(line, position.line());
    }
}
