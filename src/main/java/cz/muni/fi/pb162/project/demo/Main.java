package cz.muni.fi.pb162.project.demo;

import cz.muni.fi.pb162.project.Color;
import cz.muni.fi.pb162.project.Player;
import cz.muni.fi.pb162.project.Chess;

import java.io.IOException;

/**
 * Class for running main method.
 *
 * @author Alzbeta Strompova
 */
public class Main {


    /**
     * Runs the code.
     *
     * @param args command line arguments, will be ignored.
     */
    public static void main(String[] args) throws IOException {
        var game = new Chess(new Player("Mat", Color.WHITE), new Player("Pat", Color.BLACK));
        while (true) {
            try {
                game.play();
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }
    }
}
