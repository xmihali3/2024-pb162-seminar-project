package cz.muni.fi.pb162.project;

import cz.muni.fi.pb162.project.exceptions.MissingPlayerException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Objects;

/**
 * Class representing chess game
 *
 * @author Jakub Mihálik
 */
public class Chess extends Game implements GameWritable {
    /**
     * Creates a chess game
     *
     * @param playerOne Player of the game
     * @param playerTwo Other player of the game
     */
    public Chess(Player playerOne, Player playerTwo) {
        super(playerOne, playerTwo);
        setInitialSet();
    }

    /**
     * Creates a chess game
     *
     * @param playerOne Player of the game
     * @param playerTwo Other player of the game
     * @param board game board
     */
    public Chess(Player playerOne, Player playerTwo, Board board) {
        super(playerOne, playerTwo, board);
    }

    private void setInitialSet() {
        getBoard().putPieceOnBoard(new Position('e', 1), new Piece(Color.WHITE, PieceType.KING));
        getBoard().putPieceOnBoard(new Position('d', 1), new Piece(Color.WHITE, PieceType.QUEEN));
        getBoard().putPieceOnBoard(new Position('a', 1), new Piece(Color.WHITE, PieceType.ROOK));
        getBoard().putPieceOnBoard(new Position('h', 1), new Piece(Color.WHITE, PieceType.ROOK));
        getBoard().putPieceOnBoard(new Position('b', 1), new Piece(Color.WHITE, PieceType.KNIGHT));
        getBoard().putPieceOnBoard(new Position('g', 1), new Piece(Color.WHITE, PieceType.KNIGHT));
        getBoard().putPieceOnBoard(new Position('c', 1), new Piece(Color.WHITE, PieceType.BISHOP));
        getBoard().putPieceOnBoard(new Position('f', 1), new Piece(Color.WHITE, PieceType.BISHOP));

        getBoard().putPieceOnBoard(new Position('e', 8), new Piece(Color.BLACK, PieceType.KING));
        getBoard().putPieceOnBoard(new Position('d', 8), new Piece(Color.BLACK, PieceType.QUEEN));
        getBoard().putPieceOnBoard(new Position('a', 8), new Piece(Color.BLACK, PieceType.ROOK));
        getBoard().putPieceOnBoard(new Position('h', 8), new Piece(Color.BLACK, PieceType.ROOK));
        getBoard().putPieceOnBoard(new Position('b', 8), new Piece(Color.BLACK, PieceType.KNIGHT));
        getBoard().putPieceOnBoard(new Position('g', 8), new Piece(Color.BLACK, PieceType.KNIGHT));
        getBoard().putPieceOnBoard(new Position('c', 8), new Piece(Color.BLACK, PieceType.BISHOP));
        getBoard().putPieceOnBoard(new Position('f', 8), new Piece(Color.BLACK, PieceType.BISHOP));

        for (int i = 0; i < getBoard().getSize(); i++) {
            getBoard().putPieceOnBoard(new Position(i, 1), new Piece(Color.WHITE, PieceType.PAWN));
            getBoard().putPieceOnBoard(new Position(i, 6), new Piece(Color.BLACK, PieceType.PAWN));
        }
    }

    /**
     * Moves piece on the board from old position to new position.
     * If the position is empty or not viable nothing happens.
     * Promotes pawn to queen if it reaches the opposite site of the board.
     *
     * @param oldPosition targeted piece on board
     * @param newPosition new position for the piece
     */
    @Override
    public void move(Position oldPosition, Position newPosition) {
        Piece piece = getBoard().getPiece(oldPosition);
        super.move(oldPosition, newPosition);
        if (piece != getBoard().getPiece(oldPosition)
                && piece.getPieceType() == PieceType.PAWN
                && newPosition.line() == (piece.getColor() == Color.WHITE ? 7 : 0)) {
            getBoard().putPieceOnBoard(newPosition, new Piece(piece.getColor(), PieceType.QUEEN));
        }
    }

    /**
     * Updates the status of the game.
     * If only one player has king, he wins.
     */
//    @Override
//    protected void updateStatus() {
//        Piece[] pieces = getBoard().getAllPiecesFromBoard();
//        boolean whiteKing = false;
//        boolean blackKing = false;
//        for (Piece piece : pieces) {
//            if (piece.getPieceType() == PieceType.KING) {
//                if (piece.getColor() == Color.WHITE) {
//                    whiteKing = true;
//                } else {
//                    blackKing = true;
//                }
//            }
//        }
//        if (!whiteKing) {
//            setStateOfGame(StateOfGame.BLACK_PLAYER_WIN);
//        }
//        if (!blackKing) {
//            setStateOfGame(StateOfGame.WHITE_PLAYER_WIN);
//        }
//    }
    @Override
    protected void updateStatus() {
        Piece[] pieces = getBoard().getAllPiecesFromBoard();
        boolean whiteKing = Arrays.stream(pieces)
                .anyMatch(piece -> piece.getPieceType() == PieceType.KING && piece.getColor() == Color.WHITE);
        boolean blackKing = Arrays.stream(pieces)
                .anyMatch(piece -> piece.getPieceType() == PieceType.KING && piece.getColor() == Color.BLACK);
        if (!whiteKing) {
            setStateOfGame(StateOfGame.BLACK_PLAYER_WIN);
        }
        if (!blackKing) {
            setStateOfGame(StateOfGame.WHITE_PLAYER_WIN);
        }
    }



    @Override
    public void write(OutputStream os) throws IOException {
        os.write(getHeader().getBytes());
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (j != 0) {
                    os.write(";".getBytes());
                }
                Piece piece = getBoard().getPiece(new Position(i, j));
                if (piece == null) {
                    os.write("_".getBytes());
                } else {
                    os.write((piece.getPieceType().toString() + ',' + piece.getColor()).getBytes());
                }
                if (j == 7 && i != 7) {
                    os.write('\n');
                }
            }
        }
    }

    @Override
    public void write(File file) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(file)) {
            write(fos);
        }
    }

    /**
     * Creates header for writing functions
     *
     * @return header in string form
     */
    private String getHeader() {
        StringBuilder players = new StringBuilder();
        if (getPlayerOne().color() == Color.WHITE) {
            players.append(getPlayerOne().name()).append('-').append(getPlayerOne().color()).append(';')
                    .append(getPlayerTwo().name()).append('-').append(getPlayerTwo().color()).append('\n');
        } else {
            players.append(getPlayerTwo().name()).append('-').append(getPlayerTwo().color()).append(';')
                    .append(getPlayerOne().name()).append('-').append(getPlayerOne().color()).append('\n');
        }
        return players.toString();
    }

    /**
     * Builder subclass for building chess game
     *
     * @author Jakub Mihálik
     */
    public static class Builder extends GameBuilder<Chess> implements GameReadable {

        @Override
        public Chess build() {
            if (Objects.isNull(getPlayer1()) || Objects.isNull(getPlayer2())) {
                throw new MissingPlayerException();
            }
            return new Chess(getPlayer1(), getPlayer2(), getBoard());
        }

        @Override
        public Builder read(InputStream is) throws IOException {
            try {
                int input;
                StringBuilder pieceString;
                String[] piece;
                for (int i = 0; i < 8; i++) {
                    for (int j = 0; j < 8; j++) {
                        pieceString = new StringBuilder();
                        input = is.read();
                        while ((char) input != ';'
                               && (char) input != '\n'
                               && input != -1) {
                            pieceString.append((char) input);
                            input = is.read();
                        }
                        if (!pieceString.toString().equals("_")) {
                            piece = pieceString.toString().split(",");
                            addPieceToBoard(new Position(i, j),
                                            new Piece(Color.valueOf(piece[1].trim()), PieceType.valueOf(piece[0])));
                        }
                    }
                }
            } catch (Exception e) {
                throw new IOException();
            }
            return this;
        }

        @Override
        public Builder read(InputStream is, boolean hasHeader) throws IOException {
            try {
                if (hasHeader) {
                    StringBuilder headerString = new StringBuilder();
                    int input = is.read();
                    while ((char) input != '\n' && input != -1) {
                        headerString.append((char) input);
                        input = is.read();
                    }
                    String[] players = headerString.toString().split(";");
                    for (String player : players) {
                        String[] newPlayer = player.split("-");
                        addPlayer(new Player(newPlayer[0], Color.valueOf(newPlayer[1].trim())));
                    }
                }
            } catch (Exception e) {
                throw new IOException();
            }
            return read(is);
        }

        @Override
        public Builder read(File file) throws IOException {
            try (InputStream inputStream = new FileInputStream(file)) {
                return read(inputStream);
            }
        }

        @Override
        public Builder read(File file, boolean hasHeader) throws IOException {
            try (InputStream inputStream = new FileInputStream(file)) {
                return read(inputStream, hasHeader);
            }
        }
    }
}
