package cz.muni.fi.pb162.project.exceptions;

/**
 * Exception representing missing player
 *
 * @author Jakub Mihálik
 */
public class MissingPlayerException extends RuntimeException {

    /**
     * Constructor for new MissingPlayerException with predetermined message
     */
    public MissingPlayerException() {
        super("Missing player.");
    }

    /**
     * Constructor for new MissingPlayerException with desired message
     *
     * @param message desired error message
     */
    public MissingPlayerException(String message) {
        super(message);
    }
}