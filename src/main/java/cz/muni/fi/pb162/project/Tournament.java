package cz.muni.fi.pb162.project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class representing recorded games.
 *
 * @author Jakub Mihálik
 */
public class Tournament {
    private final Map<Game, List<Board>> record;

    /**
     * Creates tournament (record of games)
     */
    public Tournament() {
        record = new HashMap<>();
    }

    /**
     * Records current board state of given game
     *
     * @param game game to record
     */
    public void storeGameState(Game game) {
        record.putIfAbsent(game, new ArrayList<>());
        record.get(game).add(game.getBoard().makeClone());
    }

    /**
     * Finds all boards (history) of given game
     *
     * @param game game from which we want history
     * @return the full board history of the game, if the game is not recorded it will return empty array
     */
    public Collection<Board> getGameHistory(Game game) {
        return Collections.unmodifiableCollection(record.getOrDefault(game, Collections.emptyList()));
    }

    /**
     * Finds all games including given player
     *
     * @param name name of a player
     * @return all games the player is in
     */
//    public Collection<Game> findGamesOfPlayer(String name) {
//        ArrayList<Game> games = new ArrayList<>();
//        for (Game game : record.keySet()) {
//            if (game.getPlayerOne().name().equals(name) || game.getPlayerTwo().name().equals(name)) {
//                games.add(game);
//            }
//        }
//        return games;
//    }
    public Collection<Game> findGamesOfPlayer(String name) {
        return record.keySet().stream()
                .filter(game -> game.getPlayerOne().name().equals(name) || game.getPlayerTwo().name().equals(name))
                .collect(Collectors.toList());
    }
}
