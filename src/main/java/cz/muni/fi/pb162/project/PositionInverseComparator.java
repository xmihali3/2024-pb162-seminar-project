package cz.muni.fi.pb162.project;

import java.util.Comparator;

/**
 * Class representing inverse position comparator.
 *
 * @author Jakub Mihálik
 */
public class PositionInverseComparator implements Comparator<Position> {
    @Override
    public int compare(Position position1, Position position2) {
        return position2.compareTo(position1);
    }
}
