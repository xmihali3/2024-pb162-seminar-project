package cz.muni.fi.pb162.project.exceptions;

/**
 * Exception representing invalid position
 *
 * @author Jakub Mihálik
 */
public class EmptySquareException extends Exception {

    /**
     * Constructor for new EmptySquareException with predetermined message
     */
    public EmptySquareException() {
        super("The square is empty.");
    }

    /**
     * Constructor for new EmptySquareException with desired message
     *
     * @param message desired error message
     */
    public EmptySquareException(String message) {
        super(message);
    }
}