package cz.muni.fi.pb162.project.exceptions;

/**
 * Exception representing invalid format of user input
 *
 * @author Jakub Mihálik
 */
public class InvalidFormatOfInputException extends RuntimeException {

    /**
     * Constructor for new InvalidFormatOfInputException with predetermined message
     */
    public InvalidFormatOfInputException() {
        super("Invalid format of input.");
    }

    /**
     * Constructor for new InvalidFormatOfInputException with desired message
     *
     * @param message desired error message
     */
    public InvalidFormatOfInputException(String message) {
        super(message);
    }
}