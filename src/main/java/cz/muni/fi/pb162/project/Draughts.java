package cz.muni.fi.pb162.project;

import cz.muni.fi.pb162.project.exceptions.MissingPlayerException;

import java.util.Objects;

/**
 * Class representing draughts game
 *
 * @author Jakub Mihálik
 */
public class Draughts extends Game {
    /**
     * Creates a draughts game
     *
     * @param playerOne Player of the game
     * @param playerTwo Other player of the game
     */
    public Draughts(Player playerOne, Player playerTwo) {
        super(playerOne, playerTwo);
        setInitialSet();
    }

    private Draughts(Player playerOne, Player playerTwo, Board board) {
        super(playerOne, playerTwo, board);
    }

    private void setInitialSet() {
        for (int i = 0; i < getBoard().getSize(); i += 2) {
            getBoard().putPieceOnBoard(new Position(i, 0), new Piece(Color.WHITE, PieceType.DRAUGHTS_MAN));
            getBoard().putPieceOnBoard(new Position(i, 2), new Piece(Color.WHITE, PieceType.DRAUGHTS_MAN));
            getBoard().putPieceOnBoard(new Position(i, 6), new Piece(Color.BLACK, PieceType.DRAUGHTS_MAN));
        }
        for (int i = 1; i < getBoard().getSize(); i += 2) {
            getBoard().putPieceOnBoard(new Position(i, 1), new Piece(Color.WHITE, PieceType.DRAUGHTS_MAN));
            getBoard().putPieceOnBoard(new Position(i, 5), new Piece(Color.BLACK, PieceType.DRAUGHTS_MAN));
            getBoard().putPieceOnBoard(new Position(i, 7), new Piece(Color.BLACK, PieceType.DRAUGHTS_MAN));
        }
    }

    /**
     * Moves piece on the board from old position to new position.
     * If the position is empty or not viable nothing happens.
     * Promotes man to king if it reaches the opposite site of the board.
     *
     * @param oldPosition targeted piece on board
     * @param newPosition new position for the piece
     */
    @Override
    public void move(Position oldPosition, Position newPosition) {
        Piece piece = getBoard().getPiece(oldPosition);
        super.move(oldPosition, newPosition);
        if (piece != getBoard().getPiece(oldPosition)
                && piece.getPieceType() == PieceType.DRAUGHTS_MAN
                && newPosition.line() == (piece.getColor() == Color.WHITE ? 7 : 0)) {
            getBoard().putPieceOnBoard(newPosition, new Piece(piece.getColor(), PieceType.DRAUGHTS_KING));
        }
    }

    /**
     * Updates the status of the game.
     * If only one player has any pieces, he wins.
     */
    @Override
    protected void updateStatus() {
        Piece[] pieces = getBoard().getAllPiecesFromBoard();
        boolean white = false;
        boolean black = false;
        for (Piece piece : pieces) {
            if (piece.getColor() == Color.WHITE) {
                white = true;
            } else {
                black = true;
            }
        }
        if (!white) {
            setStateOfGame(StateOfGame.BLACK_PLAYER_WIN);
        }
        if (!black) {
            setStateOfGame(StateOfGame.WHITE_PLAYER_WIN);
        }
    }

    /**
     * Builder subclass for building Draughts game
     *
     * @author Jakub Mihálik
     */
    public static class Builder extends GameBuilder<Draughts>{

        @Override
        public Draughts build() {
            if (Objects.isNull(getPlayer1()) || Objects.isNull(getPlayer2())) {
                throw new MissingPlayerException();
            }
            return new Draughts(getPlayer1(), getPlayer2(), getBoard());
        }
    }
}
